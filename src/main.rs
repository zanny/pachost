/* Copyright (C) 2017 Matthew Scheirer <matt.scheirer@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;

use std::path::{Path, PathBuf};
use rocket::response::NamedFile;
use rocket_contrib::Template;

const PKGDIR: &'static str = "/var/cache/pacman/pkg/";

#[get("/")]
fn index() -> Template {
    let dir = std::fs::read_dir(Path::new(PKGDIR)).unwrap();
    let mut names = Vec::with_capacity(dir.size_hint().0);
    for entry in dir {
        // If you can't take the heat...
        names.push(entry.unwrap().file_name().into_string().unwrap());
    }

    let mut context = std::collections::HashMap::new();
    context.insert("names", &names);

    Template::render("index", &context)
}

#[get("/<file..>")]
fn get_file(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new(PKGDIR).join(file)).ok()
}

fn main() {
    rocket::ignite().mount("/", routes![index, get_file]).launch()
}
